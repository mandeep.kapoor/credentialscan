FROM ubuntu:20.04
COPY . .
COPY requirements.txt /
COPY credential.token /
COPY secretToken.txt /
RUN apt update -y && apt install -y python3-pip && apt install jq -y 
RUN pip3 install -r requirements.txt

CMD ["python3","CredentialScan.py"]
