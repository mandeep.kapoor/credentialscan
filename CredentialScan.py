import subprocess
import sys
from jira import JIRA
import jq


def detect_secrets():
    detect_secret_command = """ detect-secrets scan --hex-limit 4 --base64-limit 5 --all-files | jq -r '.results | to_entries[] | ["Filename:",.key]|@tsv' """
    p = subprocess.Popen(detect_secret_command,stdout=subprocess.PIPE,shell=True)
    s = p.communicate()[0]
    output_files = s.decode('utf-8')
    jira_username=""
    jira_api_token=""
    options = {'server' : 'https://a.atlassian.net'}
    jira = JIRA(options, basic_auth=(jira_username, jira_api_token))
    secrets_summary = "Secrets Detected in Repo"
    new_issue = jira.create_issue(project='SECOPS',summary = secrets_summary, description = output_files,issuetype={'name':'Bug'},labels=['detect_secrets'])
    print(output_files)
    return(output_files)

detect_secrets()
